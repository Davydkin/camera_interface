# -*- coding: utf-8 -*-
"""Simple grabber for o3d3xx camera.  Grabber works in separate process and
use shared array and shared values for IPC. When retrieving grabbed image,
module creates copy of shared array and returns it as numpy uin16 reshaped
array.

Image grabber requies IP address of camera and logger to print some messages.

What you can do:
- Check if image have been updated
- Get last image
- Chek if there were some errors during working loop
- Clean error flag
- Change fps
- Camera will work with FPS greater than HIGH_FPS not more than
    MAX_HIGH_FPS_SECONDS
- Camera will automaticly set low fps when exit

You can found usage example in __main__ block.
"""
import o3d3xx
import time
import numpy as np
import multiprocessing as mp
import ctypes
import logging
import signal


TIME_CONNECT = 3  # seconds
IMG_H = 132  # Height of image from camera
IMG_W = 176  # Width of image from camera
MAX_FPS = 20
HIGH_FPS = 11
MAX_HIGH_FPS_SECONDS = 5
LOW_FPS = 1


def timeout_handler(_, __):
    raise TimeoutError('Cant connect to camera')


class CameraInterface(mp.Process):
    """Interface for o3d3xx camera in separate process."""
    _instance = None

    def __new__(cls, address, logger, *args, **kwargs):
        if not CameraInterface._instance:
            CameraInterface._instance = super(CameraInterface, cls).__new__(cls, *args, **kwargs)
        return CameraInterface._instance

    @classmethod
    def getInstance(cls):
        return cls._instance

    def __init__(self, address='192.168.2.30', logger=None, *args, **kwargs):
        """Create values for IPC."""
        super().__init__(*args, **kwargs)
        if logger is None:
            self._logger = logging.getLogger('camera_interface')
        else:
            self._logger = logger
        self._img_array = mp.Array(ctypes.c_uint16, IMG_H*IMG_W)
        self._img_updated = mp.Value(ctypes.c_int, 0)
        self._working = mp.Value(ctypes.c_int, 0)
        self._error_found = mp.Value(ctypes.c_int, 0)
        self._fps_to_set = mp.Value(ctypes.c_int, 0)
        self._address = address
        self._temperature = mp.Value(ctypes.c_float, 0)
        self._temperature_need_check = mp.Value(ctypes.c_int, 0)

    def start(self):
        """Set values for working and only after it start thread."""
        with self._working.get_lock():
            self._working.value = 1
        with self._img_updated.get_lock():
            self._img_updated.value = 0
        super().start()
        self._logger.info('Process started')

    def clean_error(self):
        """Set error flag to zero."""
        self._error_found.value = 0

    def _send_fps_cmd(self, camera, fps):
        session = camera.requestSession()
        session = camera.session
        edit = session.setOperatingMode(1)
        edit = session.edit
        application = edit.editApplication(1)
        application = edit.application
        imagerConfig = application.imagerConfig
        # Change fps
        imagerConfig.setParameter('FrameRate', fps)
        # Save & Close
        application.save()
        edit.stopEditingApplication()
        session.setOperatingMode(0)
        session.cancelSession()

    def run(self):
        """Рабочий цикл."""
        # У нас есть TIME_CONNECT секунд, чтобы подключиться к камере, иначе вылезет TimeoutError
        signal.signal(signal.SIGALRM, timeout_handler)
        signal.alarm(TIME_CONNECT)
        try:
            pcic = o3d3xx.ImageClient(self._address, 50010)
            pcic.debug = False
            camera = o3d3xx.Device(self._address)
        except Exception:
            self._error_found.value = 1
            return

        # Если подключились успешно, нужно отловить ошибку, и затем продолжить работу
        t_start = time.time()
        while time.time() < t_start + TIME_CONNECT:
            try:
                time.sleep(TIME_CONNECT)
            except TimeoutError:
                break

        # Create working variables
        # Shared memory
        shared_img = np.frombuffer(self._img_array.get_obj(), dtype='uint16')
        high_fps_now = False
        high_fps_start_time = 0

        # Main workin loop
        while True:
            # Check if should stop
            with self._working.get_lock():
                if self._working.value == 0:
                    break

            # Check if need to change fps
            if self._fps_to_set.value > 0:
                fps = self._fps_to_set.value
                self._fps_to_set.value = 0
                self._send_fps_cmd(camera, fps)
                if fps >= HIGH_FPS:
                    high_fps_now = True
                    high_fps_start_time = time.time()
                else:
                    high_fps_now = False

            # Check if high too long high FPS
            if high_fps_now and time.time() > \
                    high_fps_start_time + MAX_HIGH_FPS_SECONDS:
                self._send_fps_cmd(camera, LOW_FPS)
                high_fps_now = False

            # Check temperature
            if self._temperature_need_check.value == 1:
                self._temperature.value = float(
                    camera.getParameter('TemperatureIllu'))
                self._temperature_need_check.value = 0

            # Try to get new image
            try:
                result = pcic.readNextFrame()
            except OSError:
                self._error_found.value = 1
                break

            # Update image
            with self._img_array.get_lock():
                shared_img[:] = np.frombuffer(
                    result['distance'], dtype='uint16')
                with self._img_updated.get_lock():
                    self._img_updated.value = 1

        # Close connection
        self._send_fps_cmd(camera, LOW_FPS)
        pcic.close()

    def set_fps(self, fps):
        """Change fps of camera."""
        assert type(fps) == int, 'FPS should be int'
        assert fps > 0, 'FPS should be greater than zero'
        assert fps < MAX_FPS, 'FPS should be lesser than '+str(MAX_FPS)
        # We can skip getting lock
        self._fps_to_set.value = fps

    def get_temperature(self):
        """Get temperature of camera."""
        # We can skip getting lock
        self._temperature_need_check.value = 1
        while self._temperature_need_check.value != 0:
            time.sleep(0.01)
        return self._temperature.value

    def is_updated(self):
        """If image have been updated."""
        with self._img_updated.get_lock():
            tmp = self._img_updated.value
        return bool(tmp)

    def is_error(self):
        """If there is some errors in process."""
        with self._error_found.get_lock():
            tmp = self._error_found.value
        return bool(tmp)

    def get(self, ignore_flag=False):
        """Get last image got."""
        with self._img_array.get_lock():
            with self._img_updated.get_lock():
                result = np.copy(
                    self._img_array.get_obj()).reshape(
                        (IMG_H, IMG_W)).astype(np.uint16)
                if self._img_updated.value == 0:
                    self._logger.warning('Getting not updated image')
                if not ignore_flag:
                    self._img_updated.value = 0
        return result

    def __del__(self):
        self.stop()

    def stop(self):
        """Stop working."""
        self._working.value = 0
        if self.is_alive():
            self.join(1.5)
        if self.is_alive():
            self.terminate()


if __name__ == "__main__":
    import numpy as np
    import time
    import matplotlib.pyplot as plt
    MAX = 1000

    gr = CameraInterface('192.168.2.30', None)
    gr.start()

    plt.figure()

    while True:
        try:
            time.sleep(0.5)

            img = gr.get()
            # Process image (clip it)
            img = (img < MAX) * img + (img >= MAX) * MAX

            plt.imshow(img, cmap='gray')
            plt.show()
        except KeyboardInterrupt:
            break
    gr.set_fps(1)
    gr.stop()
